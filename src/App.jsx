import React from 'react';
// vendors
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import { Layout } from 'antd';
// components
import Header from './components/header';
import Home from './components/home';
import Upload from './components/upload';
import Orders from './components/orders'
import Footer from './components/footer';

import 'antd/dist/antd.css';

const { Content } = Layout;

function App() {
  return (
    <Layout>
      <Router>
        <Header />
        <Content style={{
          height: '85.5vh',
          padding: '1rem'
        }}>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route exact path='/upload' component={Upload}/>
            <Route exact path='/orders' component={Orders}/>
          </Switch>
        </Content>
        <Footer />
      </Router>
    </Layout>
  );
}

export default App;
