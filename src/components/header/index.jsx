import React from 'react';
// vendors
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';

import 'antd/lib/layout/style/css';

const AntHeader = Layout.Header;
const { Item } = Menu;

const LINKS = [
  {
    to: '/',
    text: '商城'
  },
  {
    to: '/orders',
    text: '订单'
  },
  {
    to: '/upload',
    text: '添加商品'
  }
];

export default function Header(props) {
  return (
    <AntHeader>
      <Menu
        theme='dark'
        mode='horizontal'
        defaultSelectedKeys={['0']}
        style={{ lineHeight: '64px' }}
      >
        {
          LINKS.map((link, index) => <Item key={index}><Link to={link.to}>{link.text}</Link></Item>)
        }
      </Menu>
    </AntHeader>
  );
}
