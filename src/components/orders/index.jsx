import React, { Component } from 'react';
// vendors
import { connect } from 'react-redux';
import { Table, Divider, Tag, Button } from 'antd';
// actions
import { getAllGoodsAction } from '../../redux/actions/goodsActions';
import { getAllOrders, deleteOrder } from '../../redux/actions/ordersActions';

import './index.scss';

class Orders extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);

    this.COLUMNS = [
      {
        title: '名称',
        dataIndex: 'name',
        key: 'name'
      },
      {
        title: '单价',
        dataIndex: 'price',
        key: 'price'
      },
      {
        title: '数量',
        dataIndex: 'amount',
        key: 'amount'
      },
      {
        title: '单位',
        dataIndex: 'unit',
        key: 'unit'
      },
      {
        title: '操作',
        dataIndex: 'delete',
        key: 'delete',
        render: (text, item) => <Button type='danger' onClick={() => this.handleClick(text, item)}>删除</Button>
      }
    ];
  }

  handleClick(text, item) {
    let goodsId = this.props.goods.filter(v => v.name === item.name)[0].id;
    let orderId = this.props.order.filter(v => v.goodsId === goodsId);
    this.props.deleteOrder(
      orderId,
      () => {
        this.props.getAllOrders()
      }
    )
  }
  
  componentDidMount() {
    this.props.getAllGoods();
    this.props.getAllOrders();
  }
  
  render() {
    const { goods, orders } = this.props;
    let data = this.props.orders ? processData(orders, goods) : [];
    console.info(data)
    return (
      <div>
        <Table columns={this.COLUMNS} dataSource={data} />
      </div>
    );
  }
}

function processData(orders, goods) {
  let result = orders.reduce((acc, cur, index) => {
    let goodsInfo = goods.filter(v => v.id === cur.goodsId);
    acc.push({
      key: index,
      name: goodsInfo[0].name,
      price:goodsInfo[0].price,
      amount: cur.amount,
      unit: goodsInfo[0].unit,
      delete: ''
    });
    return acc;
  }, []);

  return result;
}

const mapStateToProps = state => ({
  goods: state.goods.goods,
  orders: state.orders.orders
});

const mapDispatchToProps = dispatch => ({
  getAllOrders: () => dispatch(getAllOrders()),
  getAllGoods: () => dispatch(getAllGoodsAction()),
  deleteOrder: (orderId, callback) => dispatch(deleteOrder(orderId, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(Orders);