import React, { Component } from 'react';
// vendors
import {
  Input,
  Button,
  message
} from 'antd';
import { connect } from 'react-redux';
// actions
import { uploadGoodsAction } from '../../redux/actions/goodsActions';
import './index.scss';

const INPUT = [
  {
    key: 'name',
    value: '',
    description: '名称：'
  },
  {
    key: 'price',
    value: '',
    description: '价格：'
  },
  {
    key: 'unit',
    value: '',
    description: '单位：'
  },
  {
    key: 'image',
    value: '',
    description: '图片：'
  }
];

class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      value: '',
      unit: '',
      image: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(e, key) {
    const value = e.target.value;
    this.setState({
      [key]: value
    });
  }

  handleClick() {
    this.props.uploadGoods(this.state,
      () => {
        this.props.history.push('/');
        message.success('商品上传成功');
      }
    );
  }

  render() {
    return (
      <>
        <h1>添加商品</h1>
        <div className='uploadContainer'>
          {
            INPUT.map((input, index) => <section key={index}>
              <div className='uploadDescription'>
                {input.description}
              </div>
              <Input
                type='text'
                size='large'
                className='uploadInput'
                value={this.state[input.key]}
                onChange={(e) => this.handleChange(e, input.key)}
              />
            </section>)
          }
        </div>
        <Button type='primary' onClick={this.handleClick}>提交</Button>
      </>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  uploadGoods: (goodsInfo, callback) => dispatch(uploadGoodsAction(goodsInfo, callback))
});

export default connect(() => ({}), mapDispatchToProps)(Upload);