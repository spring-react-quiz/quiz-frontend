import React from 'react';
// vendors
import { Layout } from "antd";

const AntFooter = Layout.Footer;

export default function Footer() {
  return (
    <AntFooter style={{
      backgroundColor: 'white',
      textAlign: 'center'
    }}>
      TW Mall ©2018 Created by LiJiahao
    </AntFooter>
  );
}
