import React, { Component } from 'react';
// vendors
import { connect } from 'react-redux';
import { Card, Icon, message } from 'antd';
// actions
import { getAllGoodsAction } from '../../redux/actions/goodsActions';
import { createOrderAction } from '../../redux/actions/ordersActions';

import './index.scss';
const { Meta } = Card;

class Home extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.props.getAllGoods();
  }

  handleClick(goodsId, amount) {
    const orderInfo = {
      goodsId,
      amount
    };
    this.props.createOrder(
      orderInfo,
      () => {
        message.success('添加订单成功');
      }
    );
  }

  render() {
    return (
      <div className='goodsContainer'>
        {
          this.props.goods ?
            this.props.goods.map((item, index) =>
              <Card
                key={index}
                style={{ width: 200 }}
                cover={
                  <img
                    alt="Sorry, something wrong happened!"
                    src={item.image}
                  />
                }
                actions={[
                  <Icon style={{
                      textAlign: 'right',
                      paddingRight: '1rem',
                      fontSize: '1.5rem'
                    }}
                    type="plus-circle"
                    onClick={ () => this.handleClick(item.id, 1)}
                  />
                ]}
              >
                <Meta
                  title={item.name}
                  description={`单价：${item.price}/${item.unit}`}
                />
              </Card>):
            null
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  goods: state.goods.goods
});

const mapDispatchToProps = dispatch => ({
  getAllGoods: () => dispatch(getAllGoodsAction()),
  createOrder: (orderInfo, callback) => dispatch(createOrderAction(orderInfo, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);