import { combineReducers } from "redux";
// reducers
import goods from './goodsReducer';
import orders from './ordersReucer';

const reducers = combineReducers({
  goods,
  orders
});
export default reducers;