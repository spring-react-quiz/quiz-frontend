import { UPLOAD_GOODS_ACTION, GET_ALL_GOODS } from "../actions/goodsActions";

const initState = {
  goods: []
};

export default function goods(state = initState, action) {
  switch (action.type) {
    case UPLOAD_GOODS_ACTION:
      return {
        ...state
      };
    case GET_ALL_GOODS:
      return {
        ...state,
        goods: action.payload
      };
    default:
      return state;
  }
}