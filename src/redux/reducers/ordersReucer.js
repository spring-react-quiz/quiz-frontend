import { CREATE_ORDER, GET_ALL_ORDERS, DELETE_ORDER } from '../actions/ordersActions';

const initState = {
  orders: []
};

export default function ordersReducer (state = initState, action) {
  switch (action.type) {
    case CREATE_ORDER:
      return {
        ...state
      };
    case GET_ALL_ORDERS:
      return {
        ...state,
        orders: action.payload
      };
    case DELETE_ORDER:
      return {
        ...state
      };
    default:
      return state;
  }
}