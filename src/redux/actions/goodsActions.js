import Axios from 'axios';

const UPLOAD_GOODS_ACTION = 'UPLOAD_GOODS_ACTION';
const GET_ALL_GOODS = 'GET_ALL_GOODS';

const uploadGoodsAction = (goodsInfo, callback) => dispatch => {
  Axios.post(
      'http://localhost:8080/api/goods',
      goodsInfo)
    .then(response => {
      if (response.status === 201) {
        dispatch({
          type: UPLOAD_GOODS_ACTION
        });
        callback();
      }
    })
    .catch(error => new Error(error));
};

const getAllGoodsAction = () => dispatch => {
  Axios.get('http://localhost:8080/api/goods')
    .then(response => {
      if (response.status === 200) {
        dispatch({
          type: GET_ALL_GOODS,
          payload: response.data
        });
      }
    })
    .catch(error => new Error(error));
};

export {
  UPLOAD_GOODS_ACTION,
  GET_ALL_GOODS,
  uploadGoodsAction,
  getAllGoodsAction
};