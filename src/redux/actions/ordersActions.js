import Axios from "axios";

const CREATE_ORDER = 'CREATE_ORDER';
const GET_ALL_ORDERS = 'GET_ALL_ORDERS';
const DELETE_ORDER = 'DELETE_ORDER';

const createOrderAction = (orderInfo, callback) => dispatch => {
  Axios.post(
    'http://localhost:8080/api/orders',
    orderInfo)
    .then(response => {
      if (response.status === 201) {
        dispatch({
          type: CREATE_ORDER
        });
        callback();
      }
    })
    .catch(error => new Error(error));
};

const getAllOrders = () => dispatch => {
  Axios.get('http://localhost:8080/api/orders')
    .then(response => {
      if (response.status === 200) {
        dispatch({
          type: GET_ALL_ORDERS,
          payload: response.data
        });
      }
    })
    .catch(error => new Error(error));
};

const deleteOrder = (orderId, callback) => dispatch => {
  Axios.delete(`http://localhost:8080/api/order/${orderId}`)
    .then(response => {
      if (response.status === 200) {
        dispatch({
          type: DELETE_ORDER
        });
        callback();
      }
    })
    .catch(error => new Error(error));
};

export {
  CREATE_ORDER,
  GET_ALL_ORDERS,
  DELETE_ORDER,
  createOrderAction,
  getAllOrders,
  deleteOrder
};