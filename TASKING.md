# TASKING

1. Config dev environment ✅

   - Frontend

     | Dependencies           |
     | ---------------------- |
     | React-router-dom       |
     | Redux                  |
     | React-redux            |
     | And-design             |
     | @testing-library/react |

   - Backend

     | Configuration |
     | ------------- |
     | h2            |
     | Mysql         |
     | Flyway        |

2. Implement upload goods feature in backend ✅

   `Uri:/api/goods(POST)`

   | Field | Type   |
   | ----- | ------ |
   | id    | Long   |
   | name  | String |
   | unit  | String |
   | image | String |

3. Implement Home Page and Upload Page ✅

4. Implement get all goods feature in backend ✅

   `Uri:/api/goods(GET)`

5. Fill data to Home Page ✅

6. Implement add goods to order feture ✅

   `Uri:/api/order(PATCH)`

   | Field   | Type |
   | ------- | ---- |
   | id      | Long |
   | goodsId | Long |
   | amount  | Long |

7. Implement add goods to order feature in frontend ✅

8. Implement get all orders feature in backend ✅

   `Uri:/api/orders(GET)`

9. Implement show all orders feature in frontend ✅

10. Implement delete specified order backend ✅

    `Uri:/api/orders(DELETE)`

11. Implement delete specified order in frontend ✅

12. Add css to frontend